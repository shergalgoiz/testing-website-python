*** Settings ***
Documentation       Testing MercuryTours Website
Resource    ../lib/keyword/register.robot

Library             Selenium2Library
Library             BuiltIn

*** Variables ***
${expected}        Tên tài khoản hoặc mật khẩu không đúng!
*** Test Cases ***
TC01: Invalid user
    [Documentation]
    [Tags]
     Given Open Mercury Website in Chrome and Maximize
     Then Verify the Home page title
     And User Click on Sign-In Link
     And User Input Username
     And User Input Password Page SignIn
     And User Click Submit page SignIn
     And Verify Invalid User       ${expected}
     And Capture Page Screenshot    invalid_user_and_password.png
     And Close Browser

TC02: Login with admin role
    [Documentation]
    [Tags]
     Given Open Mercury Website in Chrome and Maximize
     Then Verify the Home page title
     And User Click on Sign-In Link
     And User Input User Admin
     And User Input Password Admin
     And User Click Submit page SignIn
     And Capture Page Screenshot    admin_screen.png
     And Close Browser

TC03: Register
    [Documentation]
    [Tags]
    Given Open Mercury Website in Chrome and Maximize
    Then Verify the Home page title
    And register.User Click on Sign-On Link
    And Verify SignOn Page title
    Then User Input Name
    And User Input DateofBirth
    And User Check sex
    And User Input Phone
    And User Input Username
    And User Input Password Page SignUp
    And User Input Repeat Password
    And User Input Email
    And User Input Address
    And User Click Submit
    And Register.User Clicks OK
    And Capture Page Screenshot    register.png
    And Close Browser

TC04: Searching
    [Documentation]
    [Tags]
    Given Open Mercury Website in Chrome and Maximize
    Then Verify the Home page title
    Then User Searching
    And Capture Page Screenshot     searching.png
    And Close Browser

TC05: Ordering and Edit Cart
    [Documentation]
    [Tags]
    Given Open Mercury Website in Chrome and Maximize
    Then Verify the Home page title
    And User Click Order
    And User Edit Cart
    And Capture Page Screenshot    Ordering_and_edit1.png
    And Close Browser

TC06: Choose kind of book
    [Documentation]
    [Tags]
    Given Open Mercury Website in Chrome and Maximize
    Then Verify the Home page title
    And User Choose Theme
    And Capture Page Screenshot     type_of_book.png
    And Close Browser

