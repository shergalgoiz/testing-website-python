*** Settings ***
Library  ../lib/db_helper.py
Suite Setup  custom_suite_setup
Suite Teardown  custom_suite_teardown

*** Keywords ***
custom_suite_setup
    ${db_con} =       db open   Driver={SQL Server Native Client 11.0};Server=thaiphuc.mssql.somee.com;port=1433;Network Library=DBMSSOCN;Database=thaiphuc;uid=thaiphuc11cb1_SQLLogin_1;pwd=8b2ycalfut;
    Set Global Variable      ${db_con}

custom_suite_teardown
    db close

*** Test Cases ***
TCDB01: Drop table
    [Tags]    db    smoke
    ${output} =    db_execute    ${db_con}    IF OBJECT_ID('dbo.test', 'U') IS NOT NULL DROP TABLE dbo.test;
    Log    ${output}

TCDB02: Create table
    [Tags]    db    smoke
    ${output} =    db_execute    ${db_con}  CREATE TABLE test (id integer unique, first_name varchar(20), last_name varchar(20));
    Log    ${output}

TCDB03: Insert sample data
    [Tags]    db    smoke
    ${output} =    db_execute_script    ${db_con}    ../test_insertData.sql
    Log    ${output}
    Should Be Equal As Strings    ${output}    2

TCDB04: Check exits
    [Tags]    db    smoke
    ${output} =    db_fetch_first    ${db_con}    SELECT * FROM KhachHang WHERE HoTen = 'b';
    Log    ${output}

TCDB05: Check relation insert
    [Tags]    db    smoke
    ${output} =    db_execute_script    ${db_con}    ../check_relation.sql

#cannot delete beccause object has value in ThamGia
#cannot delect because conflict reference table ThamGia
TCDB06: Check relation delete
    [Tags]    db    smoke
    ${output} =    db_execute_script    ${db_con}    ../test_insertData.sql

TCDB07: Check not exits
    [Tags]    db    smoke
    ${output} =    db_fetch_first    ${db_con}    SELECT TaiKhoan FROM KhachHang WHERE HoTen = 'test';
    Log    ${output}
    Should Be Equal As Strings    ${output}    None

TCDB08: Row count
     [Tags]    db    smoke
    ${output} =    db_count    ${db_con}    SELECT MaKH FROM KhachHang;
#    ${output} =    db_count    ${db_con}    SELECT MaSach FROM Sach;
    Log    ${output}
    Should Be Equal As Strings    ${output}    24
