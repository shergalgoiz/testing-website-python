try:
    from SeleniumLibrary.base import keyword, LibraryComponent
    from SeleniumLibrary.keywords import ElementKeywords
    from SeleniumLibrary.keywords import FormElementKeywords, WaitingKeywords
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.support.ui import Select
    from selenium.webdriver import ActionChains
    import time
    import datetime

    # from PIL import ImageGrab

except ImportError as imp_err:
    print('Errors in importing files - From %s'%__file__)
    print('\n---{{{ Failed - ' + format(imp_err) + ' }}}---\n')
    raise

class ElementHandler(LibraryComponent):
    @keyword
    def click_yes_on_dialog(self):
        obj = self.driver.switch_to.alert
        print("Following message is show : %s" % obj.text)
        obj.accept()

    @keyword
    def click_no_on_dialog(self):
        obj = self.driver.switch_to.alert
        print("Following message is shown : %s" % obj.text)
        obj.dismiss()

    @keyword
    def click_OK_on_dialog(self):
        self.driver.switch_to.alert.accept()