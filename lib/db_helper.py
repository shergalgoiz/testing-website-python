import pyodbc
class db_helper():
    def __init__(self):
        self.conn = None

    def db_open(self, connection_string):
        self.conn = pyodbc.connect(connection_string)
        return self.conn

    def db_close(self):
        self.conn.close()

    def db_execute(self, conn, cmd):
        cursor = conn.cursor()
        return cursor.execute(cmd)

    def db_execute_script(self, conn, path):
        c = 0
        with open(path, 'r') as f:
            sql = f.read().strip().split(';')
            for cmd in sql:
                cmd = cmd.strip()
                if not cmd:
                    continue
                _t = self.db_execute(conn, cmd)
                c += _t.rowcount
        return c

    def db_fetch_first(self, conn, cmd):
        return self.db_execute(conn, cmd).fetchone()

    def db_count(self, conn, cmd):
        result = self.db_execute(conn,cmd).fetchall()
        return len(result)
