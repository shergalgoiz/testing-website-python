*** Settings ***
Library  ../core.py
#core đây nè a
*** Variables ***
${UserAdmin}        admin
${PasswordAdmin}    123456
${URL}              http://bookcorner.somee.com/
${title}            Sách giá rẻ
${timeout}          3s
${Name}             Admin
${DatofBirth}       23/12/1990
${Phone}            21321321
${UserName}         Admin123
${Password}         Test1234
${RepeatPass}       Test1234
${Mail}             zxczxczx@gmail.com
${Address}          czxczcx
${wrong_user}        xpath://*[@id="NoiDung"]/h4[2]
${Searching}        Hành Trình Về Phương Đông
${amount}           3
${amount2}           -2
#${expected_value}


*** Keywords ***
User Input username and password
    Click Element       xpath=//*[@id="ToanTrang"]/section[1]/div[1]/div[2]/a[3]

Open Mercury Website in Chrome and Maximize
    Open Browser  url=${URL}  browser=chrome
    Maximize Browser Window
    Log  Browser is Maximized
    Log to console  Browser is Maximized

Verify the Home page title
    Title Should Be  ${title}
    Log to console  Home Page Title Verified

User Click on Sign-On Link
    Click Element  xpath=//*[@id="ToanTrang"]/section[1]/div[1]/div[2]/a[2]
    Log to console  Clicked on Signon link successfully.

User Click on Sign-In Link
    Click Element  xpath=//*[@id="ToanTrang"]/section[1]/div[1]/div[2]/a[3]
    Log to console  Clicked on SignIn link successfully.

User Click Submit page SignIn
    Click Element   id=btnSubmit
    Sleep   10s

User Click Add Book
    Click Element   xpath=//*[@id="content"]/p/a

Verify Invalid User
    [Arguments]  ${expected_value}
    ${value}=   Get Text    ${wrong_user}
    Should be equal   ${expected_value}    ${value}

Verify SignOn Page title
    ${WindowTitle}=    Get Title
    Should Be Equal  ${WindowTitle}  Sách giá rẻ
    Log to console  Navigated to :${WindowTitle}

User Input Name
    Input Text    id=txtHoTen  ${Name}
    Log to console  ${Name}

User Input DateofBirth
    Input Text    id=NgaySinh   ${DatofBirth}
    Log to console  ${DatofBirth}

User Check sex
    Click Element   xpath=//*[@id="nu"]

User Input Phone
    Input Text    id=txtDienThoai  ${Phone}
    Log to console  ${Phone}

User Input User Admin
    Input Text    id=txtTaiKhoan  ${UserAdmin}
    Log to console  ${UserName}

User Input Password Admin
    Input Text    id=txtMatKhau  ${PasswordAdmin}
    Log to console  ${Password}

User Input Username
    Input Text    id=txtTaiKhoan  ${UserName}
    Log to console  ${UserName}

User Input Password Page SignIn
    Input Text    id=txtMatKhau  ${Password}
    Log to console  ${Password}

User Input Password Page SignUp
    Input Text    id=MatKhau  ${Password}
    Log to console  ${Password}

User Input Repeat Password
    Input Text    id=XacNhanMatKhau  ${RepeatPass}
    Log to console  ${RepeatPass}

User Input Email
    Input Text    id=txtEmail  ${Mail}
    Log to console  ${Mail}

User Input Address
    Input Text    id=txtDiaChi  ${Address}
    Log to console  ${Address}

User Click Submit
    Click Element    xpath=//*[@id=" XacNhan"]
    log     Done

User Clicks OK
    Handle Alert   action=ACCEPT
    Sleep   5s

User Searching
    sleep  3s
    Input Text    id=txtTimKiem  ${Searching}
    Click Element   xpath=//*[@id="divHinhAnhTimKiem"]/button

User Click Order
    Sleep   1s
    Click Element   xpath=//*[@id="btnGioHang"]
    sleep  33
    Click Element   xpath=//*[@id="NoiDung"]/div[3]/div/ul/li[3]/a
    Click Element   xpath=//*[@id="btnGioHang"]
    Click Element   xpath=//*[@id="ToanTrang"]/section[1]/div[1]/div[2]/a[1]

User edit cart
    Click Element   xpath=//*[@id="divGioHang"]/tbody/tr[4]/td/a
    Input Text    xpath=//*[@id="txtSoLuong"]   ${amount}
    Click Element   xpath=//*[@id="divGioHang"]/tbody/tr[2]/td[7]/input
    Click Element   xpath=//*[@id="divGioHang"]/tbody/tr[4]/td/a
    Input Text    xpath=//*[@id="txtSoLuong"]   ${amount2}
    Click Element   xpath=//*[@id="divGioHang"]/tbody/tr[2]/td[7]/input
#    Click Element   //*[@id="divGioHang"]/tbody/tr[5]/td/form/input
#    Input Text      xpath=//*[@id="txtTaiKhoan"]        ${UserName}
#    Input Text      xpath=//*[@id="txtMatKhau"]         ${Password}
#    Click Element   xpath=//*[@id="btnSubmit"]


User Choose Theme
    Click Element   xpath=//*[@id="btnChuDe"]


#Enter Invalid Password
#    Input Text  //input[@name='password']  ${Password}
#    Log to console  Entered wrong password
#
#Click on Submit
#    Click Element  //input[ @ name = 'login']
#    Log to console  Clicking on Submit